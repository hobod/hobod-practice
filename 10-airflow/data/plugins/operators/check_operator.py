from airflow.models import BaseOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook


class CheckOperator(BaseOperator):

    template_fields = ("check_table",)
    ui_color = "#0000ff"

    def __init__(self,
                 check_table,
                 **kwargs):

        self.check_table = check_table,

        self._query = """
        SELECT magType, load_date
        FROM {tbl_name}
        GROUP BY magType, load_date
        HAVING count(1) > 1;
        """.format(tbl_name=check_table)

        self._pg_conn = "results_db"

        super().__init__(**kwargs)

    def execute(self, context):

        self.log.info(f"Checking table {self.check_table}.")
        hook = PostgresHook(postgres_conn_id=self._pg_conn)
        conn = hook.get_conn()
        self.log.info(f"Successfully connected to {self._pg_conn} database.")
        with conn.cursor() as cur:
            cur.execute(self._query)
            errors = cur.fetchall()
            if errors:
                raise ValueError(f"Following keys violate unique constraint: {errors}")
        self.log.info("Success")
        self.log.info("All records are correct!")
