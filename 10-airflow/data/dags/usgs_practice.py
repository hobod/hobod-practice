import logging
from datetime import datetime, timedelta
from io import StringIO

import pandas as pd
import requests
from airflow.hooks.base import BaseHook
from airflow.models import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.sensors.python import PythonSensor

from operators.check_operator import CheckOperator

RESULTS_TABLE = "USGS"
CREATE_SQL = """
    CREATE TABLE IF NOT EXISTS {tbl_name} (
        magType TEXT,
        mag DOUBLE PRECISION,
        latitude DOUBLE PRECISION,
        longitude DOUBLE PRECISION,
        load_date TEXT,
        unique(magType, load_date)
    )
""".format(tbl_name=RESULTS_TABLE)


INSERT_SQL = """
    INSERT INTO {tbl_name} (magType, mag, latitude, longitude, load_date)
    VALUES
        {values}
    ON CONFLICT (magType, load_date)
    DO UPDATE
        SET mag = EXCLUDED.mag,
            latitude = EXCLUDED.latitude,
            longitude = EXCLUDED.longitude;
"""


def process_data(text):
    data = StringIO(text)
    df = pd.read_csv(data)
    df_flt = df.loc[(df['type'] == 'earthquake') & (df['mag'].notnull()) & (df['magType'].notnull())]
    df_flt['rank'] = df_flt.groupby('magType')['mag'].rank(method='first', ascending=False)
    df_final = df_flt.loc[df_flt['rank'] == 1][['magType', 'mag', 'latitude', 'longitude']]
    return df_final.to_dict(orient='records')


def get_row_count(**kwargs):
    conn = BaseHook.get_connection(kwargs['conn_id'])
    url = f"{conn.host}{kwargs['endpoint']}"
    logging.info(f"Sending GET request to USGS API: {url}. Parameters: {kwargs['params']}")
    resp = requests.get(url, kwargs['params'])
    if resp.status_code == 200:
        logging.info("Success.")
        return int(resp.text) > 0
    else:
        logging.error(f"Failure on getting response from USGS API: {resp.status_code}\n{resp.text}")
        return False


def get_data(**kwargs):
    conn = BaseHook.get_connection(kwargs['conn_id'])
    url = f"{conn.host}{kwargs['endpoint']}"
    logging.info(f"Sending GET request to USGS API: {url}. Parameters: {kwargs['params']}")
    resp = requests.get(url, kwargs['params'])
    if resp.status_code == 200:
        logging.info("Success.")
        data = process_data(resp.text)
        return data
    else:
        raise ValueError(f"Failure on getting response from USGS API: {resp.status_code}\n{resp.text}")


def load_data_to_pg(**kwargs):
    hook = PostgresHook(postgres_conn_id=kwargs['conn_id'])
    conn = hook.get_conn()
    logging.info("Successfully connected to Postgres DB")
    with conn.cursor() as cur:
        ti = kwargs['ti']
        data = ti.xcom_pull(task_ids="get_new_data", key="return_value")
        logging.info("Successfully retrieved data from XCom")
        rows = map(
            lambda r: f"('{r['magType']}', {r['mag']}, {r['latitude']}, {r['longitude']}, '{kwargs['load_date']}')",
            data
        )
        values = '\n,    '.join(rows)
        query = INSERT_SQL.format(tbl_name=kwargs['result_table'], values=values)
        cur.execute(query)
    conn.commit()
    logging.info("Rows inserted successfully.")
    conn.close()


def_args = {
    "owner": "Airflow",
    "email": "some.person@some.domain.com",
    "email_on_retry": False,
    "email_on_failure": False,
    "retries": 1,
    "retry_delay": 30,
    "depends_on_past": False,
    "start_date": datetime(2024, 4, 1),
    "end_date": datetime(2024, 4, 10)
}

with DAG(
    dag_id="usgs_practice_dag",
    description="Ежедневная загрузка данных о землетрясениях из USGS API",
    tags=["example", "practice", "usgs"],
    schedule="@daily",
    default_args=def_args,
    max_active_runs=1,
    concurrency=1,
    user_defined_macros={
        "get_start_date": lambda dt: dt.strftime("%Y-%m-%d"),
        "get_end_date": lambda dt: (dt + timedelta(days=1)).strftime("%Y-%m-%d")
    }
):

    # 0. Get dates for which to retrieve data:
    START_DATE = "{{ get_start_date(execution_date) }}"
    END_DATE = "{{ get_end_date(execution_date) }}"

    # 1. Create table to store results:
    ddl_operator = PostgresOperator(
        task_id="create_results_table",
        postgres_conn_id="results_db",
        sql=CREATE_SQL
    )

    # 2. Check if there is data available for current date:
    check_if_data_exists = PythonSensor(
        task_id="check_if_data_exists",
        python_callable=get_row_count,
        op_kwargs={
            "conn_id": "usgs_api",
            "endpoint": "count",
            "params": {
                "starttime": START_DATE,
                "endtime": END_DATE
            }
        },
        poke_interval=60,
        timeout=300,
        soft_fail=True,
        mode='reschedule'
    )

    # 3. Get data from API and write it into results table:
    #       - select only earthquakes
    #       - magnitude should not be empty
    #       - find record with max magnitude per each magType
    get_new_data = PythonOperator(
        task_id="get_new_data",
        python_callable=get_data,
        op_kwargs={
            "conn_id": "usgs_api",
            "endpoint": "query",
            "params": {
                "format": "csv",
                "starttime": START_DATE,
                "endtime": END_DATE
            }
        },
        do_xcom_push=True
    )

    # 4. Load new data to Database:
    load_data = PythonOperator(
        task_id="load_data",
        python_callable=load_data_to_pg,
        op_kwargs={
            "conn_id": "results_db",
            "load_date": START_DATE,
            "result_table": RESULTS_TABLE
        },
        do_xcom_push=False
    )

    # 5. Check results (custom operator if we will have time)
    check_data = CheckOperator(
        task_id="check_data",
        check_table=RESULTS_TABLE
    )

    # 6. Link tasks in order
    ddl_operator >> check_if_data_exists >> get_new_data >> load_data >> check_data


# UI LOGIN:
# username: student
# password: E20D6AmUW17IO1M7
