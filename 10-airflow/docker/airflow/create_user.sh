#!/bin/bash

host=$1
port=$2
admin_user=$3
admin_password=$4
user_login=$5
user_password=$6
user_first_name=$7
user_last_name=$8
user_email=$9

# create user with role Op
curl -X POST "http://$host:$port/api/v1/users" \
-H 'Content-Type: application/json' \
--user "$admin_user:$admin_password" \
-d "{
  \"first_name\": \"$user_first_name\",
  \"last_name\": \"$user_last_name\",
  \"username\": \"$user_login\",
  \"email\": \"$user_email\",
  \"password\": \"$user_password\",
  \"roles\": [{\"name\":  \"Op\"}]
}"