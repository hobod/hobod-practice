#!/bin/bash

source ../../.env

./base_setup.sh localhost 8888 $_AIRFLOW_WWW_USER_USERNAME $_AIRFLOW_WWW_USER_PASSWORD
./create_user.sh localhost 8888 $_AIRFLOW_WWW_USER_USERNAME $_AIRFLOW_WWW_USER_PASSWORD \
  $_AIRFLOW_STUDENT_USERNAME $_AIRFLOW_STUDENT_PASSWORD Student Student 'student@student.edu'