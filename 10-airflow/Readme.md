# Docker Environment Setup

Create `.env` file in `10-airflow` directory with following variables:

```bash
AIRFLOW_UID=1011                  # set airflow uid to your user uid so you could access airflow data folders.
AIRFLOW_GID=0
_PG_AIRFLOW_PASSWORD=???          # PostgreSQL password for airflow user.
_PG_STUDENT_PASSWORD=???          # PostgreSQL password for student user. 
_AIRFLOW_WWW_USER_USERNAME=admin 
_AIRFLOW_WWW_USER_PASSWORD=???    # Password for Airflow UI admin user.
_AIRFLOW_STUDENT_USERNAME=student
_AIRFLOW_STUDENT_PASSWORD=???     # Password for Airflow UI student user.
```