## Apache Spark

Будем работать в JuPyter notebook'e:
1. Логинимся с пробросом портов: `ssh hob2020XX@mipt-client.atp-fivt.org -L 8088:mipt-master:8088 -L 18089:mipt-master:18089`

2. Копируем себе код ноутбука и запускаем (семантика команды запуска будет пояснена позже).
```
cp -r /home/velkerr/seminars/hobod2019/14-15-spark/06-spark-base_nb.ipynb /home/velkerr/seminars/hobod2019/14-15-spark/images ~/
PYSPARK_DRIVER_PYTHON=jupyter PYSPARK_PYTHON=/usr/bin/python3 PYSPARK_DRIVER_PYTHON_OPTS='notebook --ip="*" --port=<PORT> --NotebookApp.token="<TOKEN>" --no-browser' pyspark2 --master=yarn --num-executors=2
```
   * Выбираем себе порт начиная с 30000.

3. Открыть в браузере (локально): localhost:PORT.

4. Для семинара по Datadrames: `/home/velkerr/seminars/hobod2019/14-15-spark/spark_df`